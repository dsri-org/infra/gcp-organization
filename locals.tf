# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  deployment = "gcp-bootstrap"
  name       = "${var.environment}-${local.deployment}"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  environments = ["staging", "production"]

  gitlab_project_paths = distinct(flatten([
    for project in local.projects : [
      for service_account in project.service_accounts : "dsri-org/infra/gcp/${service_account.repository}"
    ]
  ]))

  gitlab_projects_used_by = distinct(flatten([
    for project in local.projects : flatten([
      for service_account in project.service_accounts : [
        for user in(can(service_account.used_by) ? service_account.used_by : []) : {
          full_path = "dsri-org/infra/gcp/${service_account.repository}"
          user      = "dsri-org/infra/gcp/${user}"
        }
      ]
    ])
  ]))

  gitlab_projects_used_by_map = {
    for project in local.gitlab_projects_used_by : "${project.full_path}:${project.user}" => project
  }

  gitlab_group_paths = distinct([
    for project in local.gitlab_project_paths : dirname(project)
  ])

  projects = yamldecode(file("${path.module}/data/projects.yml"))

  projects_with_environment = flatten([
    for environment in local.environments : [
      for project in local.projects : merge(project, {
        name        = "${environment}-${project.name}"
        environment = environment
      })
    ]
  ])

  projects_map = {
    for project in local.projects_with_environment : project.name => project
  }
}
