# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "google_folder" "folders" {
  for_each = toset(local.environments)

  display_name = each.key
  parent       = data.google_organization.org.id
}

module "projects" {
  source = "./modules/project"

  for_each = local.projects_map

  billing_account_id = var.google_cloud_billing_account_id

  environment      = each.value.environment
  name             = each.value.name
  gitlab_projects  = gitlab_project.this
  folder_id        = google_folder.folders[each.value.environment].id
  services         = each.value.services
  service_accounts = each.value.service_accounts
}

resource "gitlab_project" "this" {
  for_each = toset(local.gitlab_project_paths)

  name         = basename(each.key)
  namespace_id = data.gitlab_group.this[dirname(each.key)].group_id

  allow_merge_on_skipped_pipeline                  = false
  analytics_access_level                           = "disabled"
  auto_cancel_pending_pipelines                    = "enabled"
  autoclose_referenced_issues                      = true
  build_git_strategy                               = "fetch"
  builds_access_level                              = "enabled"
  ci_default_git_depth                             = 1
  ci_forward_deployment_enabled                    = true
  ci_restrict_pipeline_cancellation_role           = "developer"
  ci_separated_caches                              = false
  container_registry_access_level                  = "disabled"
  environments_access_level                        = "enabled"
  feature_flags_access_level                       = "disabled"
  infrastructure_access_level                      = "enabled"
  initialize_with_readme                           = false
  issues_access_level                              = "enabled"
  issues_enabled                                   = true
  lfs_enabled                                      = false
  merge_method                                     = "merge"
  merge_pipelines_enabled                          = false
  merge_requests_access_level                      = "enabled"
  merge_trains_enabled                             = false
  monitor_access_level                             = "disabled"
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  packages_enabled                                 = true
  pages_access_level                               = "disabled"
  printing_merge_request_link_enabled              = true
  public_jobs                                      = false
  releases_access_level                            = "disabled"
  remove_source_branch_after_merge                 = true
  repository_access_level                          = "enabled"
  request_access_enabled                           = false
  requirements_access_level                        = "disabled"
  resolve_outdated_diff_discussions                = true
  restrict_user_defined_variables                  = true
  security_and_compliance_access_level             = "disabled"
  snippets_access_level                            = "disabled"
  snippets_enabled                                 = false
  squash_option                                    = "always"
  visibility_level                                 = "public"
  wiki_access_level                                = "disabled"
  wiki_enabled                                     = false

  push_rules {
    commit_committer_check = true
    deny_delete_tag        = true
    max_file_size          = 1
    member_check           = true
    prevent_secrets        = true
  }
}

resource "gitlab_project_job_token_scope" "used_by" {
  for_each = local.gitlab_projects_used_by_map

  project           = gitlab_project.this[each.value.full_path].id
  target_project_id = gitlab_project.this[each.value.user].id
}
