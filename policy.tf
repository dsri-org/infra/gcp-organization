# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "google_organization_policy" "allowed_regions" {
  org_id     = data.google_organization.org.org_id
  constraint = "constraints/gcp.resourceLocations"

  list_policy {
    allow {
      values = [
        "in:us-central1-locations",
        "in:us-west1-locations",
      ]
    }
  }
}

resource "google_organization_policy" "allowed_services" {
  org_id     = data.google_organization.org.org_id
  constraint = "constraints/gcp.restrictServiceUsage"

  list_policy {
    allow {
      values = [
        "accesscontextmanager.googleapis.com",
        "cloudasset.googleapis.com",
        "cloudbilling.googleapis.com",
        "cloudkms.googleapis.com",
        "cloudresourcemanager.googleapis.com",
        "compute.googleapis.com",
        "container.googleapis.com",
        "containersecurity.googleapis.com",
        "dns.googleapis.com",
        "iam.googleapis.com",
        "iap.googleapis.com",
        "identitytoolkit.googleapis.com", # Identity Platform
        "logging.googleapis.com",
        "networkmanagement.googleapis.com",
        "osconfig.googleapis.com",
        "redis.googleapis.com",
        "securitycenter.googleapis.com",
        "servicedirectory.googleapis.com",
        "servicenetworking.googleapis.com",
        "serviceusage.googleapis.com",
        "sqladmin.googleapis.com",
        "sql-component.googleapis.com",
        "storage.googleapis.com",
        "storagetransfer.googleapis.com",
      ]
    }
  }
}


resource "google_organization_policy" "boolean_policies" {
  for_each = toset([
    "constraints/cloudkms.disableBeforeDestroy",

    "constraints/compute.requireOsConfig",
    "constraints/compute.requireOsLogin",
    "constraints/compute.skipDefaultNetworkCreation",

    "constraints/essentialcontacts.disableProjectSecurityContacts",

    # this actually prevents the automatic IAM grants even though it is worded
    # as the opposite
    "constraints/iam.automaticIamGrantsForDefaultServiceAccounts",

    "constraints/iam.disableServiceAccountKeyUpload",

    "constraints/sql.restrictAuthorizedNetworks",
    "constraints/sql.restrictPublicIp",

    "constraints/storage.publicAccessPrevention",
    "constraints/storage.secureHttpTransport",
    "constraints/storage.uniformBucketLevelAccess",
  ])

  org_id     = data.google_organization.org.org_id
  constraint = each.key

  boolean_policy {
    enforced = true
  }
}
