# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

#
# The resources in this file provision an organization-wide, read-only service
# account that enables an integration with ULRI IT's Crowdstrike CSPM service.
#
resource "google_service_account" "crowdstrike_cspm" {
  account_id  = "crowdstrike-cspm"
  description = "Crowdstrike CSPM"
}

resource "google_organization_iam_member" "crowdstrike_cspm" {
  for_each = toset([
    "roles/appengine.appViewer",
    "roles/browser",
    "roles/cloudasset.viewer",
    "roles/firebaseappcheck.viewer",
    "roles/firebaseauth.viewer",
    "roles/firebasedatabase.viewer",
  ])
  member = google_service_account.crowdstrike_cspm.member
  org_id = data.google_organization.org.org_id
  role   = each.key
}
