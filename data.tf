# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

data "google_organization" "org" {
  domain = "dsri.org"
}

data "gitlab_group" "this" {
  for_each = toset(local.gitlab_group_paths)

  full_path = each.key
}
