# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "google_cloud_service_account_file" {
  type      = string
  default   = "credentials.json"
  sensitive = true
}

variable "google_cloud_billing_account_id" {
  type      = string
  sensitive = true
}
