# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "root" {
  source = "../.."

  environment                       = "production"
  google_cloud_service_account_file = var.google_cloud_service_account_file
  google_cloud_billing_account_id   = var.google_cloud_billing_account_id
}
