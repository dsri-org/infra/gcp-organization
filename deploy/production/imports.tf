# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import {
  id = "projects/dsri-org-bootstrap/serviceAccounts/crowdstrike-cspm@dsri-org-bootstrap.iam.gserviceaccount.com"
  to = module.root.google_service_account.crowdstrike_cspm
}
