# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  roles_map = {
    for role in var.roles : role.name => role
  }
}
