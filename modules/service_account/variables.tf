# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "project_id" {
  type = string
}

variable "gitlab_project_id" {
  type = string
}

variable "name" {
  type = string
}

variable "repository" {
  type = string
}

variable "environment" {
  type = string
}

variable "roles" {
  type = list(object({
    name = string
  }))
}
