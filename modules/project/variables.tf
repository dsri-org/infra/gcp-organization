# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "name" {
  type = string
}

variable "environment" {
  type = string
}

variable "gitlab_projects" {}

variable "folder_id" {
  type = string
}

variable "billing_account_id" {
  type = string
}

variable "services" {
  type = list(string)
}

variable "service_accounts" {
  type = list(object({
    repository = string
    roles = list(object({
      name = string
    }))
  }))
}
