# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "random_string" "this" {
  length  = 6
  lower   = false
  upper   = false
  special = false
}

resource "google_project" "this" {
  name                = "${var.name}-${random_string.this.result}"
  project_id          = "${var.name}-${random_string.this.result}"
  billing_account     = var.billing_account_id
  folder_id           = var.folder_id
  auto_create_network = false
}

resource "google_project_service" "this" {
  for_each = toset(var.services)

  project = google_project.this.id
  service = each.key
}

module "service_accounts" {
  for_each = local.service_accounts_map

  source = "../service_account"

  project_id        = google_project.this.project_id
  gitlab_project_id = var.gitlab_projects["dsri-org/infra/gcp/${each.value.repository}"].id
  name              = basename(each.value.repository)
  repository        = each.value.repository
  environment       = var.environment

  roles = each.value.roles
}
